package com.example.fragmentcolorapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.fragmentcolorapp.databinding.FragmentButtonBinding

class ButtonFragment : Fragment() {

    private var _binding: FragmentButtonBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentButtonBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.cvBlue.setOnClickListener{findNavController().navigate(R.id.action_buttonFragment_to_blueFragment)}
        binding.cvGray.setOnClickListener{findNavController().navigate(R.id.action_buttonFragment_to_grayFragment)}
        binding.cvGreen.setOnClickListener{findNavController().navigate(R.id.action_buttonFragment_to_greenFragment)}
        binding.cvPurple.setOnClickListener{findNavController().navigate(R.id.action_buttonFragment_to_purpleFragment)}
        binding.cvRed.setOnClickListener{findNavController().navigate(R.id.action_buttonFragment_to_redFragment)}
        binding.cvYellow.setOnClickListener{findNavController().navigate(R.id.action_buttonFragment_to_yellowFragment)}
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}